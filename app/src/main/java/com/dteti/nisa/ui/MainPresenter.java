package com.dteti.nisa.ui;

class MainPresenter implements MainContract.Presenter {

    private MainContract.MainView mainView;

    MainPresenter(MainContract.MainView mainView) {
        this.mainView = mainView;
    }

    void initActivity() {
        mainView.initToolbar();
        mainView.initBottomNavigationView();
        mainView.initFragment();
    }
}
