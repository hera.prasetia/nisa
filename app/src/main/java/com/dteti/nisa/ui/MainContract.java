package com.dteti.nisa.ui;

import android.content.Context;

public interface MainContract {

    interface MainView {
        Context getContext();

        void initToolbar();

        void initBottomNavigationView();

        void initFragment();
    }

    interface Presenter{

    }

}
