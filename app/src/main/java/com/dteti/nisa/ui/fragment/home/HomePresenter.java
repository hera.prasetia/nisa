package com.dteti.nisa.ui.fragment.home;

class HomePresenter implements HomeContract.Presenter {

    private HomeContract.HomeView homeView;

    HomePresenter(HomeContract.HomeView homeView) {
        this.homeView = homeView;
    }

    void initFragment(){
        homeView.initAdapter();
    }


}
