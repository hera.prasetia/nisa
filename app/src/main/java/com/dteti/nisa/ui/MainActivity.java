package com.dteti.nisa.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.dteti.nisa.R;
import com.dteti.nisa.ui.fragment.bookmark.BookmarkFragment;
import com.dteti.nisa.ui.fragment.date.DateFragment;
import com.dteti.nisa.ui.fragment.home.HomeFragment;
import com.dteti.nisa.ui.fragment.setting.SettingFragment;
import com.dteti.nisa.util.BottomNavigationViewHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.MainView, BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    MainPresenter presenter;

    final Fragment fragment1 = new HomeFragment();
    final Fragment fragment2 = new DateFragment();
    final Fragment fragment3 = new BookmarkFragment();
    final Fragment fragment4 = new SettingFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragment1;

    @BindView(R.id.navigation_home)
    View navigationHome;
    @BindView(R.id.navigation_date)
    View navigationDate;
    @BindView(R.id.navigation_bookmark)
    View navigationBookmark;
    @BindView(R.id.navigation_setting)
    View navigationSetting;
    @BindView(R.id.bottom_navigation_view)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
        presenter.initActivity();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void initToolbar() {
        setSupportActionBar(toolbar);
    }

    @Override
    public void initBottomNavigationView() {
        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public void initFragment() {
        fm.beginTransaction().add(R.id.fl_fragment_container, fragment4, "4").hide(fragment4).commit();
        fm.beginTransaction().add(R.id.fl_fragment_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.fl_fragment_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.fl_fragment_container, fragment1, "1").commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                goToHomeFragment();
                return true;
            case R.id.navigation_date:
                goToDateFragment();
                return true;
            case R.id.navigation_bookmark:
                goToBookmarkFragment();
                return true;
            case R.id.navigation_setting:
                goToSettingFragment();
                return true;
        }
        return false;
    }

    private void goToHomeFragment() {
        fm.beginTransaction().hide(active).show(fragment1).commit();
        active = fragment1;
    }

    private void goToDateFragment() {
        fm.beginTransaction().hide(active).show(fragment2).commit();
        active = fragment2;
    }

    private void goToBookmarkFragment() {
        fm.beginTransaction().hide(active).show(fragment3).commit();
        active = fragment3;
    }

    private void goToSettingFragment() {
        fm.beginTransaction().hide(active).show(fragment4).commit();
        active = fragment4;
    }
}
