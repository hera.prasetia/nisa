package com.dteti.nisa.ui.fragment.home;

public interface HomeContract {

    interface HomeView {
        void initAdapter();

    }

    interface Presenter {

    }
}
