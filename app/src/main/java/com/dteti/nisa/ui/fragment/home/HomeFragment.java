package com.dteti.nisa.ui.fragment.home;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dteti.nisa.R;
import com.dteti.nisa.ui.fragment.home.agenda.AgendaFragment;
import com.dteti.nisa.ui.fragment.home.akademik.AkademikFragment;
import com.dteti.nisa.ui.fragment.home.perkuliahan.PerkuliahanFragment;
import com.google.android.material.tabs.TabLayout;

public class HomeFragment extends Fragment implements HomeContract.HomeView {

    private HomePresenter presenter;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        presenter = new HomePresenter(this);
        presenter.initFragment();

        return view;
    }

    @Override
    public void initAdapter() {
        HomeAdapter adapter = new HomeAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(new AgendaFragment(), "Agenda");
        adapter.addFragment(new AkademikFragment(), "Akademik");
        adapter.addFragment(new PerkuliahanFragment(), "Perkuliahan");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
